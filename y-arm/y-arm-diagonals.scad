/** Diagonal cutouts for y-arm **/

/*
Material-saving and access to hot-end.
*/
module frontAngleCutout () {
    polyhedron([
        [-midX, -midY+30, -midZ - .1],
        [-midX, -midY, -midZ - .1],
        [-midX+42, -midY, -midZ - .1],
        [-midX, -midY+30, midZ + .1],
        [-midX, -midY, midZ + .1],
        [-midX+42, -midY, midZ + .1]
    ],
    [
        [5,4,3],
        [2,1,4,5],
        [0,2,5,3],
        [1,0,3,4],
        [0,1,2]
    ]);
}

module backAngleCutout () {
    //y:46 x:31.5
    translate([
        midX - 130, -midY + 48.5
    ])
    polyhedron([
        [midX, -midY + 46, -midZ - 0.1],
        [midX -31.5, -midY, -midZ - 0.1],
        [midX, -midY, -midZ - 0.1],
        [midX, -midY + 46, midZ + 0.1],
        [midX -31.5, -midY, midZ + 0.1],
        [midX, -midY, midZ + 0.1]
    ],
    [
        [5,4,3],
        [2,1,4,5],
        [0,2,5,3],
        [1,0,3,4],
        [0,1,2]
    ]);
}
