/* Y-arm Constants */

/**
full length of piece
**/

ARM_LENGTH = 260;
// max height
ARM_HEIGHT = 98;
// max depth
ARM_DEPTH = 0.5;

/**
Cutouts
**/

// long cutout length
LONG_CUTOUTS_LENGTH = 152;
// From left edge of arm to cutouts
LONG_CUTOUTS_LEFT_OFFSET = 65;

// height of cutout for bottom smooth rod
BOTTOM_LONG_CUTOUT_HEIGHT = 21.5;
// height of cutout for y cam
MIDDLE_LONG_CUTOUT_HEIGHT = 18;
// height of cutout for top smooth rod
TOP_LONG_CUTOUT_HEIGHT = 21.5;

// height of bottom edge of middle cutout for y cam
MIDDLE_LONG_CUTOUT_OFFSET = 34;
// height of top edge of top cutout
TOP_LONG_CUTOUT_OFFSET = 5;

// (At the upper-left corner of the arm is a cutout to make room and improve cooling of the extruder NEMA 17 motor.) 
// Extruder cutout x
EXTRUDER_CUTOUT_LENGTH = 40;
// Extruder cutout y
EXTRUDER_CUTOUT_HEIGHT = 55;

/**
Ghost constants
**/
// Top horizontal piece on y-arm
TOP_Y_ARM_OVERHANG = 3;
