/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Copyright (C) 2020 Daemonburrito
**/

/**
Printrbot beta project, part: Y-arm-vertical
**/

// global includes
include <MCAD/nuts_and_bolts.scad>;

// local lib
include <../lib/constants.scad>;
include <../lib/util_fns.scad>;
include <../lib/wood_joints.scad>;

// part includes
include <./constants.scad>;
include <./y-arm-fastener-holes.scad>;
include <./y-arm-diagonals.scad>;

// middle measures
midX = mid(ARM_LENGTH);
midY = mid(ARM_HEIGHT);
midZ = mid(WOOD_DEPTH);

/**
This is the infamous "y-arm" for the basic Printrbot, complete with "sag."
**/
module yArm () {
    /*
    Main rectangle of wood at beginning of process.
    */
    module yArmVertical () {
        color("wheat")
        cube([
            ARM_LENGTH,
            ARM_HEIGHT,
            WOOD_DEPTH
        ], center=true);
    }

    /*
    Bottom smooth rod and LM8UU bearings cutout

    Provides access to the 8mm smooth rod and 2xLM8UU bearings which reset aginst the Y square plate.
    */
    module bottomCutout () {
        bottomXOffset = (
            -midX
            +mid(LONG_CUTOUTS_LENGTH)
            +LONG_CUTOUTS_LEFT_OFFSET
        );

        bottomYOffset = (
            -midY
            +mid(BOTTOM_LONG_CUTOUT_HEIGHT)
        );

        translate([
            bottomXOffset,
            bottomYOffset,
            0
        ])
        cube([
            LONG_CUTOUTS_LENGTH+0.1,
            BOTTOM_LONG_CUTOUT_HEIGHT+0.1, // this can be cleaned up with translation
            WOOD_DEPTH+0.1
        ], center=true);


        /*
        Notches which hold the 8mm smooth rod up against gravity.

        Holds loosely, secured by nylon ties.
        */
        module bottomNotchLeft () {
            bnDim = [
                ROD_CUTOUT_WIDTH,
                ROD_CUTOUT_HEIGHT,
                WOOD_DEPTH+0.1
            ];

            translate([
                (bnDim.x - LONG_CUTOUTS_LEFT_OFFSET) - (bnDim.x/2) - bnDim.x,
                -midY + bnDim.y/2 + (BOTTOM_LONG_CUTOUT_HEIGHT/3),
            ])
            cube([
                ROD_CUTOUT_WIDTH,
                ROD_CUTOUT_HEIGHT,
                WOOD_DEPTH + 0.1
            ],center=true);
        }

        module bottomNotchRight () {
            bnDim = [
                ROD_CUTOUT_WIDTH,
                ROD_CUTOUT_HEIGHT,
                WOOD_DEPTH+0.1
            ];

            translate([
                (bnDim.x - LONG_CUTOUTS_LEFT_OFFSET) - (bnDim.x/2) - bnDim.x + LONG_CUTOUTS_LENGTH +bnDim.x,
                -midY + bnDim.y/2 + (BOTTOM_LONG_CUTOUT_HEIGHT/3),
            ])
            cube([
                ROD_CUTOUT_WIDTH,
                ROD_CUTOUT_HEIGHT,
                WOOD_DEPTH + 0.1
            ],center=true);
        }

        bottomNotchLeft();
        bottomNotchRight();
    }


    /**
    Middle long cutout

    This cutout leaves room for the Y-cam and drive (currently just fishing
    wire.)
    **/
    module middleCutout () {
        middleCutoutYOffset = 34;

        xOffset = (
            -midX
            +mid(LONG_CUTOUTS_LENGTH)
            +LONG_CUTOUTS_LEFT_OFFSET
        );

        yOffset = (
            -mid(ARM_HEIGHT)
            +middleCutoutYOffset
            +mid(MIDDLE_LONG_CUTOUT_HEIGHT)
        );

        translate([xOffset, yOffset])
        cube([
            LONG_CUTOUTS_LENGTH,
            18,
            WOOD_DEPTH+2
        ], center=true);
    }


    /**
    Topmost cutout for top smooth 8mm rod and bearings.

    Mass of this section held up by second section of notches against gravity.
    **/

    // Topmost cutout, leaving room for LM8UUs, and suspending top 8mm smooth rod
    module topCutout () {
        // X interior dimension of top cutout
        topCutoutHeight = 22;

        // Distance from top of vertical piece
        topYOffset = 5;

        translate([
            -LONG_CUTOUTS_LEFT_OFFSET + (LONG_CUTOUTS_LENGTH/2),
            midY-topYOffset-(topCutoutHeight/2)
        ])
        cube([
            LONG_CUTOUTS_LENGTH,
            topCutoutHeight,
            WOOD_DEPTH + 2
        ], center=true);


        /*
        Holds loosely, secured by nylon ties.
        */
        module topNotchLeft () {
            bnDim = [
                ROD_CUTOUT_WIDTH,
                ROD_CUTOUT_HEIGHT,
                WOOD_DEPTH+0.1
            ];

            translate([
                (bnDim.x
                    - LONG_CUTOUTS_LEFT_OFFSET
                )
                    -(bnDim.x/2)
                    -bnDim.x,
                midY
                    -TOP_LONG_CUTOUT_OFFSET
                    -bnDim.y/2
                    -(TOP_LONG_CUTOUT_HEIGHT/3)
            ])
            cube([
                ROD_CUTOUT_WIDTH,
                ROD_CUTOUT_HEIGHT,
                WOOD_DEPTH + 0.1
            ],center=true);
        }

        module topNotchRight () {
            bnDim = [
                ROD_CUTOUT_WIDTH,
                ROD_CUTOUT_HEIGHT,
                WOOD_DEPTH+0.1
            ];

            translate([
                (bnDim.x
                    - LONG_CUTOUTS_LEFT_OFFSET
                )
                    -(bnDim.x/2)
                    -bnDim.x + LONG_CUTOUTS_LENGTH +bnDim.x,
                midY
                    -TOP_LONG_CUTOUT_OFFSET
                    -bnDim.y/2
                    -(TOP_LONG_CUTOUT_HEIGHT/3),
            ])
            cube([
                ROD_CUTOUT_WIDTH,
                ROD_CUTOUT_HEIGHT,
                WOOD_DEPTH + 0.1
            ],center=true);
        }

        // render
        topNotchLeft();
        topNotchRight();
    }

    // Extruder motor rests here
    module extruderCutout () {
        cutoutX = 40;
        cutoutY = 54;

        translate([
            -midX + mid(cutoutX),
            midY - mid(cutoutY)
        ])
        cube([
            cutoutX + 0.1,
            cutoutY + 0.1,
            WOOD_DEPTH + 0.4
        ], center=true);

        // Imported from /y-arm/y-arm-fastener-holes
        boltHoles();


        // Wood joints surrounding extruder stepper
        module extruderPlatformWoodJointHoles () {
            module bottomLeftJointHole () {
                translate([
                    -midX + mid(cutoutX) -12,
                    -midY + mid(cutoutY) + 10,
                ])
                squareJointHole(center=true);
            }


            module bottomRightJointHole () {
                translate([
                    -midX + mid(cutoutX) + 12,
                    -midY + mid(cutoutY) + 10,
                ])
                squareJointHole(center=true);
            }


            module rightmostBottomJointHole () {
                translate([
                    -midX + 45.5,
                    -midY + 10
                ])
                squareJointHole(center=true);
            }


            module rightmostMidJointHole () {
                translate([
                    -midX + 45.5,
                    -midY + 46
                ])
                squareJointHole(center=true);
            }


            module rightmostTopJointHole () {
                translate([
                    -midX + 45.5,
                    midY - 13
                ])

                squareJointHole(center=true);
            }

            bottomLeftJointHole();
            bottomRightJointHole();
            rightmostBottomJointHole();
            rightmostMidJointHole();
            rightmostTopJointHole();
        }

        extruderPlatformWoodJointHoles();
    }


    module endstopWallBoltHole () {
        $fn = 20;

        translate([
            midX - 4,
            midY - 15,
            M3_CAP_HEIGHT + 0.4
        ])
        rotate([180,0,0])
        boltHole(3, length=20);
    }


    module endstopJointHole () {
        translate([
            midX - 4,
            midY - 25
        ])
        squareJointHole(center=true);
    }


    // These bolts and nuts hold part of the top to this
    // section.
    module verticalBolts () {
        $fn = 20;
        // centered
        // from the extruder edge
        leftVertBoltXOffset = 14;

        // from the right edge
        rightVertBoltXOffset = 20 - (3.5 / 2);


        module leftVertBoltCutout () {
            translate([
                -midX
                    +leftVertBoltXOffset
                    +EXTRUDER_CUTOUT_LENGTH,
                midY - 5
            ])
            cube([
                3.25,
                10+0.01,
                WOOD_DEPTH+1
            ], center=true);
        }


        module leftVertBoltHole () {
            translate([
                -midX
                    +leftVertBoltXOffset
                    +EXTRUDER_CUTOUT_LENGTH,
                midY - 10 + 15 + 1
            ])
            rotate([90,0,0])
            boltHole(3, length=20);
        }


        module leftVertNutCutout () {
            translate([
                -midX
                    +leftVertBoltXOffset
                    +EXTRUDER_CUTOUT_LENGTH,
                midY - 10 + 4.75
            ])
            cube([
                5.5,
                2.6,
                WOOD_DEPTH+1
            ],center=true);
        }


        module leftVertNutHole () {
            translate([
                -midX
                    +leftVertBoltXOffset
                    +EXTRUDER_CUTOUT_LENGTH,
                (ARM_HEIGHT/2) - 4
            ])
            rotate([90,90,0])
            nutHole(size=3);
        }


        module rightVertBoltCutout () {
            translate([
                midX
                    -rightVertBoltXOffset,
                midY])
            cube([
                3.5,
                20,
                WOOD_DEPTH+1
            ], center=true);
        }


        module rightVertBoltHole () {
            translate([
                midX
                    -rightVertBoltXOffset,
                midY - 10 + 15 + 1
            ])
            rotate([90,0,0])
            boltHole(3, length=20);
        }


        module rightVertNutCutout () {
            translate([
                midX
                    -rightVertBoltXOffset,
                midY 
                    -10
                    +4.75
            ])
            cube([
                5.5,
                2.6,
                WOOD_DEPTH+1
            ],center=true);
        }


        // the bolts and nuts rendered are just for reference
        module rightVertNutHole () {
            translate([
                midX
                    -rightVertBoltXOffset,
                (ARM_HEIGHT/2) - 4
            ])
            rotate([90,90,0])
            nutHole(size=3);
        }

        union () {
            leftVertBoltCutout();
            %leftVertBoltHole();
            leftVertNutCutout();
            %leftVertNutHole();
        }
        union () {
            rightVertBoltCutout();
            %rightVertBoltHole();
            rightVertNutCutout();
            %rightVertNutHole();
        }
    }

    difference () {
        yArmVertical();
            boltHoles();
            frontAngleCutout();
            backAngleCutout();
            topCutout();
            middleCutout();
            bottomCutout();
            extruderCutout();
            %endstopWallBoltHole();
            endstopJointHole();
            verticalBolts();
    }


    /** Ghost module, to be defined in y-arm-top **/
    module demoTop () {
        translate([
            EXTRUDER_CUTOUT_LENGTH/2,
            midY + (WOOD_DEPTH)- (WOOD_DEPTH/2),
            -25 + (WOOD_DEPTH/2)+TOP_Y_ARM_OVERHANG
        ])
        rotate([90,0,0])
        cube([
            ARM_LENGTH
                -EXTRUDER_CUTOUT_LENGTH,
            50,
            WOOD_DEPTH
        ],center=true);
    }
    %demoTop();
}

yArm();
