include <../lib/constants.scad>;
include <../lib/util_fns.scad>;
include <MCAD/nuts_and_bolts.scad>;

/**
boltHoles: A use of the modules `nuts_and_bolts.scad`

`boltHoles` is meant to describe many place where M3 fasteners will reside.
**/
module boltHoles () {
    $fs = 20;
    $fn = 20;

    module topExtruderWallBolthole () {
        translate([
            -(ARM_LENGTH / 2)
            +EXTRUDER_CUTOUT_LENGTH
            +5.5,
            ARM_HEIGHT / 2 - 22,
            M3_CAP_HEIGHT + 0.4
        ])
        rotate([180,0,0])

        boltHole(3, length=20);
    }

    // this bolt is 180 deg away from the others, to give
    // clearance for the drive cutout.
    module leftBeltBolthole () {
        translate([
            -midX + 58.5,
            0,
            -(WOOD_DEPTH/2) - 0.01
        ])
        boltHole(3, length=20);

            translate([
                -midX
                    +58.5,
                0,
                (WOOD_DEPTH/2)
            ])

            nutHole(size=3);
    }

    module extruderPlatformBolthole () {
        cutoutX = 40;
        cutoutY = 54;

        translate([
            -midX + mid(cutoutX),
            -midY + mid(cutoutY) + 10,
            +mid(WOOD_DEPTH)
        ])
        rotate([180,0,0])
        boltHole(3,length=20);
    }

    %leftBeltBolthole();
    %extruderPlatformBolthole();
    %topExtruderWallBolthole();
}
