**WORK IN PROGRESS**
All parts are not yet complete.

# Printrbot Beta (c. Basic) model in SCAD

This repo contains a faitful (afaict) rendition of Printrbot's original
Basic prototype. It's _mostly_ the same as the Printrbot Basic (wood) 
that was released.

## Purpose

The purpose of this little project is to keep to the ethos of FLOSS/FOSS
_hard_ware, such as it is, and this robust design won't die anytime soon.

Improvements were certainly made in later iterations, but having as much
as possible of our collective treasury of creativity catalogued and
preserved justifies making this model. And, for those who don't want to
create more e-waste (a euphemism for heavy metals and other nasties) and
have some 8mm smooth rod taking up space now that the fashion has moved to
rails and extruded aluminum, this library makes is possible to try upgrades
in OpenSCAD before taking a saw to the printer.

### Historical

These machines have a place in the history of libre FFF. There are still
clones of this design extant, and even if there werent, it's worth the bits
it takes for it to live on.

### Practical

The practical rationale is two-fold.

#### Minimal materials

As 2020 has shown countries like the U.S. in amazing clarity, designs
requiring minimal material input are valuable on their own.

* The structural components are laser-cut wood. A circular, jig, or even
  hand saw can suffice.
* It relies on small calibre smooth rod, which has an imperial mate (8mm).
* The smooth rod design only needs skate bearings (608) and LM8UUs.
* Tractive parts are fishing string held by friction to rotary tool sand
  paper on the cams of the small-ish NEMA-17s (except for the extruder
  motor, which is suprising large and capable: a Kysan 11124090.)
* The board this Beta came with was an AT90-based 128k Teensyduino. A
  surprising capable choice, but still in the < $1 range of 
  microcontrollers.
* The stepper drivers are the ubiquitous Allegro 4988s; this is maybe the
  only design flaw apart from the infamous "Y-arm sag." The board is as
  good or better than Due+Polulu in every way *except* that these are
  soldered on! However, replacing the entire board is simple and also within
  reach for nearly everyone needing FFF.
* Speaking of replacing the whole board; if you go the route of building it,
  the board doesn't need a FTDI! Kind of a welcome relief, especially with
  people of limited means getting bit periodically by computer OS's
  identifying "counterfeit" FTDIs by ID.

#### Hacking the design to use older materials

As alluded to earlier, this library makes the task of hacking the hardware
design much simpler, as changes can be made in OpenSCAD instead of with a
saw.

If you have a stack of 8mm rods in the corner of the shop, there's more mods
than I can think of for this design.
* A second tower on the opposite site of the bed.
* Increasing the dimensions of the bed.
* Increasing the gauge of the threaded Z rod, and using a proper coupler.
* Getting a "Prusa cable" and running two Z steppers.
* Altering the tool head: accomodating an optical leveler (made trivial by
  Marlin 2.0's "mesh leveling."
* And on...

Tldr; no reason to throw it away; there are many in the world who can use
machine.
