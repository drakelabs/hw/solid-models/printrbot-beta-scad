/*
Constants for the Printrbot Beta

All units metric
*/

// laser-cut wood depth
WOOD_DEPTH = 6;

// joint peg length
JOINT_PEG_LENGTH = 7;

// joint peg width
JOINT_PEG_WIDTH = 7;

M3_CAP_HEIGHT = 2.75;

// loosely holds 8mm smooth rod
ROD_CUTOUT_WIDTH = 9.5;
ROD_CUTOUT_HEIGHT = 7;