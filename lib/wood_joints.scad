/** wood joints */
use <./constants.scad>

module squareJointHole(center=true) {
    width = JOINT_PEG_WIDTH;
    length = JOINT_PEG_LENGTH;
    depth = WOOD_DEPTH + 0.1;

    cube([
        width,
        length,
        depth
    ],center=center);
}